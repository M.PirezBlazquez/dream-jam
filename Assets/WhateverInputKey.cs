using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhateverInputKey : MonoBehaviour
{

    [SerializeField]
    GameObject imag1;
    [SerializeField]
    GameObject imag2;

    // Update is called once per frame
    void Update()
    {
        if(Input.anyKey)
        {
            imag2.SetActive(true);
            imag1.SetActive(false);
        } 

    }
}
