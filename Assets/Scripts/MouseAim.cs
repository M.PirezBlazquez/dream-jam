using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MouseAim : MonoBehaviour
{
    [Header("Pivot Object")]
    public Transform pivot; // Objeto pivote

    [Header("Controlled Object")]
    public GameObject controlledObject; // Objeto que será lanzado

    [Header("Distance from Pivot")]
    public float distanceFromPivot = 2f; // Distancia constante del pivote

    private float timeButtonHeld = 0f; // Tiempo que el botón ha sido presionado
    private bool isLaunching = false; // Si está en proceso de lanzamiento
    private bool isAiming = false; // Controla si el modo de apuntar está activo
    public PlayerController PlayerController;

    public Animator animator;

    [SerializeField]
    RuntimeAnimatorController controller;
    [SerializeField]
    CapsuleCollider2D piveYBomba;

    [Header("ESCALA")]
    [SerializeField]
    Vector3 localscale;

    [Header("Aux Var")]
    public float angle = 0f;

    void Update()
    {

        if (Input.GetMouseButtonDown(1)) // Click derecho para activar el modo apuntar
        {
            animator.SetTrigger("CargarBulb");

            animator.SetBool("Aiming", true);


            controlledObject.SetActive(true);
            controlledObject.GetComponent<Rigidbody2D>().isKinematic = true;
            isAiming = true; // Comienza a apuntar
            PlayerController.canMove = false;
            
        }

        if (isAiming) // Solo apuntar si está en modo de apuntar
        {
            Aim();
        }
        if(!isAiming)
        {
         PlayerController.canMove = true;
        }

        if (Input.GetMouseButtonDown(0) && isAiming) // Comenzar a medir tiempo con el click izquierdo
        {
            timeButtonHeld = 0f;

            isLaunching = true; // Preparar para lanzar
        }

        if (Input.GetMouseButton(0) && isAiming && isLaunching) // Mantener pulsado el click izquierdo
        {
            timeButtonHeld += Time.deltaTime;
        }

        if (Input.GetMouseButtonUp(0) && isAiming && isLaunching) 
        {
            animator.SetBool("Throwing", true);
            animator.runtimeAnimatorController = controller;

            piveYBomba.enabled = false;

            LaunchObject();
            PlayerController.canMove = true;
            isLaunching = false; 
        }

        if (Input.GetMouseButtonUp(1) && !isLaunching) 
        {
            isAiming = false;

            animator.SetBool("Throwing", false);
            animator.SetBool("Aiming", false);
        }
    }

    void Aim()
    {

        Vector3 mousePos = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.transform.position.z));
        mousePos.z = pivot.position.z;

        Vector3 direction = (mousePos - pivot.position).normalized;
        angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90;

        //angle = Mathf.Clamp(angle, -30, 30);
        Vector3 newPosition = pivot.position + direction * distanceFromPivot;
        //newPosition.x = Mathf.Clamp(newPosition.x, -0.6f, 0.6f);

        controlledObject.transform.position = newPosition;
        controlledObject.transform.rotation = Quaternion.Euler(0, 0, angle);

        controlledObject.transform.localScale = localscale;

    }

    void LaunchObject()
    {
        controlledObject.transform.localScale = Vector3.one;

        float launchForce = timeButtonHeld * 5f; 
        Vector3 launchDirection = controlledObject.transform.up;         
        Rigidbody2D rb = controlledObject.GetComponent<Rigidbody2D>();
        rb.isKinematic = false; 
        rb.velocity = launchDirection * launchForce; 
        controlledObject.SetActive(true);
        isAiming = false;
        this.enabled = false;

    }

   
}
