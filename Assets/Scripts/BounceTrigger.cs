using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceTrigger : MonoBehaviour
{

    [SerializeField]
    Collider2D c2d;

    public float bounceHeight = 5.0f;  // Altura del salto
    private bool hasBounced = false;   // Controla si ya ha ocurrido un rebote

    public float resetBounce = 5f;
    public float isBouncing;


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bombilla") && !hasBounced)
        {
            Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.AddForce(Vector3.up * Mathf.Sqrt(2 * 4.9f), ForceMode2D.Impulse);

                hasBounced = true;

                GetComponent<Collider2D>().enabled = false;
            }
            else
            {
                ResetBounce();
            }
        }

        if (other.CompareTag("Player"))
        {
            Rigidbody2D rb = other.GetComponent<Rigidbody2D>();
            if (rb != null)
            {
                rb.AddForce(Vector3.up * Mathf.Sqrt(2 * 4.9f), ForceMode2D.Impulse);

                GetComponent<Collider2D>().enabled = false;

            }
            else
            {
                ResetBounce();
            }
        }
    }

    public void ResetBounce()
    {
        hasBounced = false;
        GetComponent<Collider2D>().enabled = true;
    }
}
