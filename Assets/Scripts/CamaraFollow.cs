using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamaraFollow : MonoBehaviour
{
    public Transform playerTransform;  // Referencia al transform del jugador.
    public float smoothSpeed = 0.125f;  // Velocidad con la que la cámara se alineará con la posición del jugador.
    public Vector3 offset;  // La distancia inicial entre la cámara y el jugador.

    void LateUpdate()
    {
        Vector3 desiredPosition = playerTransform.position + offset;  // Calcula la posición deseada.
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);  // Suaviza el movimiento hacia la posición deseada.
        transform.position = smoothedPosition;  // Actualiza la posición de la cámara.
    }
}


