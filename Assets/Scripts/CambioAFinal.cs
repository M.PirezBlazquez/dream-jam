using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class VideoToEndScene : MonoBehaviour
{
    public VideoPlayer videoPlayer; // Arrastra aqu� el componente VideoPlayer desde el Inspector
    public string nextSceneName = "Agradecimientos"; // Nombre de la pr�xima escena a cargar

    void Start()
    {
        // Aseg�rate de que el videoPlayer tiene un clip asignado
        if (videoPlayer.clip == null)
        {
            Debug.LogError("VideoPlayer no tiene un clip asignado");
            return;
        }

        // Configura el VideoPlayer
        videoPlayer.loopPointReached += OnVideoFinished; // Suscribir al evento que se dispara cuando el video termina
        videoPlayer.Play(); // Comienza a reproducir el video
    }

    private void OnVideoFinished(VideoPlayer vp)
    {
        // Cambiar a la siguiente escena cuando el video termine
        SceneManager.LoadScene(nextSceneName);
    }

    private void OnDestroy()
    {
        // Limpiar al destruir para evitar referencias colgantes
        if (videoPlayer != null)
        {
            videoPlayer.loopPointReached -= OnVideoFinished;
        }
    }
}
