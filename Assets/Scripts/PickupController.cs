using UnityEngine;

public class PickupController : MonoBehaviour
{
    public Transform mountPoint; // El punto donde la pelota será colocada
    public GameObject Bombilla;

    public CircleCollider2D ball; // La pelota que será recogida
    public GameObject aimControllerObject; // Objeto que contiene el script MouseAim

    private MouseAim mouseAimScript; // Referencia al script MouseAim
    public bool isPlayerNear = false;

    [SerializeField]
    Animator animator;
    [SerializeField]
    RuntimeAnimatorController controller;
    [SerializeField]
    CapsuleCollider2D piveYBomba;
    

    void OnEnable() 
    {
        // Asegúrate de que el ball tiene un Collider adecuado
        if (ball == null || !ball.isTrigger)
        {
            Debug.LogError("No se encontró un collider configurado como Trigger en la pelota.");
        }
        else
        {
            Debug.Log("Collider encontrado y configurado correctamente.");
        }

        // Obtener la referencia al script MouseAim si no está en el mismo objeto
        if (aimControllerObject != null)
        {
            mouseAimScript = aimControllerObject.GetComponent<MouseAim>();
            if (mouseAimScript == null)
            {
                Debug.LogError("No se encontró el script MouseAim en el objeto especificado.");
            }
        }
        else
        {
            Debug.LogError("No se ha asignado el objeto que contiene el script MouseAim.");
        }
    }

    void Update()
    {
        if (isPlayerNear && Input.GetKeyDown(KeyCode.E))
        {
            PickupBall();
            Bombilla.SetActive(false);

            mouseAimScript.animator.SetBool("Throwing", false);
            mouseAimScript.animator.SetBool("Aiming", false);

            animator.runtimeAnimatorController = controller;
            piveYBomba.enabled = true;

            Debug.Log("He recogido la pelota");
        }
    }



    void PickupBall()
    {
        ball.transform.position = mountPoint.position;
        ball.gameObject.SetActive(true);
        if (mouseAimScript != null)
        {
            mouseAimScript.enabled = true;
        }
        Rigidbody rb = ball.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.velocity = Vector3.zero;
            rb.angularVelocity = Vector3.zero;
        }
    }
}
