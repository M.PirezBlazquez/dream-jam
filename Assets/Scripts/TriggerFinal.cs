using UnityEngine;
using UnityEngine.Video;
using UnityEngine.SceneManagement;

public class CinematicTrigger : MonoBehaviour
{
   
    public string nextSceneName = "CinematicaFinal"; 

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bombilla"))
        {

                SceneManager.LoadScene(nextSceneName);

            
        }
    }

}
