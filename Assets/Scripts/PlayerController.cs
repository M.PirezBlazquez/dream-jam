using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Burst;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Movement Var")]
    public float speed = 5f;
    public Rigidbody2D rb;
    [Range(-1f, 1f)]
    public int lookRight;

    public int maxTimeHeld = 4; // Duraci�n m�xima del salto
    public bool canMove = true;

    [SerializeField]
    bool step = true;

    Vector3 localScale = Vector3.one;

    [Header("Animator")]
    [SerializeField]
    Animator animator;

    [Header("Sound")]
    [SerializeField]
    AudioClip clip;
    [SerializeField]
    AudioSource audioSource;

    void Update()
    {        

        if (canMove)
        {
            float move = Input.GetAxis("Horizontal");


            if (move > 0f)
            {
                lookRight = 1;
                localScale.x = 1f;

                if (step)
                {
                    StartCoroutine(DarStep());
                }
            }
            else if (move < 0f)
            {
                lookRight = -1;
                localScale.x = -1f;

                if (step)
                {
                    StartCoroutine(DarStep());
                }
            }

            transform.localScale = localScale;

            rb.velocity = new Vector2(move * speed, rb.velocity.y);            

            move = Mathf.Abs(move);

            animator.SetFloat("Move", move);

        }
        else
        {
            // Opcionalmente, detener el movimiento cuando no se permite mover
            rb.velocity = new Vector2(0, rb.velocity.y);
        }


    }

    IEnumerator DarStep()
    {
        step = false;

        audioSource.PlayOneShot(clip);
        Debug.Log("STEP");

        yield return new WaitForSeconds(.75f);

        step = true;
    }
}
