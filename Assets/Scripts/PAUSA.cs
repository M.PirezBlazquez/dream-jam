using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PAUSA : MonoBehaviour
{
    public bool juegoPausado = false;

    [SerializeField]
    GameObject mPausa;


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            juegoPausado = !juegoPausado;

            if (juegoPausado)
            {
                if (mPausa != null)
                {
                    PausarJuego();
                    mPausa.SetActive(true);
                }
                
            }
            else
            {
                if (mPausa != null)
                {
                    ReanudarJuego();
                    mPausa.SetActive(false);
                }                

            }
        }
    }

    public void PausarJuego()
    {
        Time.timeScale = 0f;
    }

    public void ReanudarJuego()
    {
        Time.timeScale = 1f;
    }

    public void ChangeMainMenu()
    {
        if(mPausa != null)
        {
            SceneManager.LoadScene("MainMenu");

        }
    }

    public void Reiniciar()
    {
        if(mPausa != null)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

}
