using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class JumpController : MonoBehaviour
{
    [SerializeField]
    PlayerController controller;

    [Header("Jump Variables")]
    public bool grounded;
    [Range(1, 10)]
    public int JumpForce = 4;
    [SerializeField]
    private float maxJumpHeight = 5f; // Altura máxima del salto en metros

    public bool isJumping = false;
    private float chargeTime = 0f;
    private const float maxChargeTime = 3f; // Tiempo máximo de carga

    [SerializeField]
    PlayerController playerController;

    [Header("Animator")]
    [SerializeField]
    Animator animator;

    private void FixedUpdate()
    {
        if (grounded)
        {
            playerController.rb.constraints = RigidbodyConstraints2D.FreezeRotation;

            playerController.enabled = true;

            isJumping = false;
            animator.SetBool("Jump", isJumping);
            controller.enabled = true;
        } else
        {
            isJumping = true;

        }
    }

    void Update()
    {
        if (Input.GetButtonDown("Jump") && !isJumping)
        {
            playerController.enabled = false;
            playerController.rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;

            StartJump();
        }

        if (Input.GetButton("Jump") && !isJumping)
        {
            playerController.enabled = false;

            if (chargeTime < maxChargeTime)
            {
                chargeTime += Time.deltaTime; // Incrementar tiempo de carga
            }
        }

        if (Input.GetButtonUp("Jump") && !isJumping)
        {
            grounded = false;

            playerController.rb.constraints = RigidbodyConstraints2D.FreezeRotation;

            playerController.enabled = false;
            PerformJump();
        }
    }

    void StartJump()
    {
        chargeTime = 0f;
        animator.SetTrigger("ChargedJump");
        animator.SetBool("Charging", true);
        controller.rb.constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    void PerformJump()
    {

        Vector2 jumpDirection = Vector2.up; // Dirección predeterminada hacia arriba

        // Calcular la fuerza de salto en función del tiempo de carga
        float scaledJumpForce = Mathf.Lerp(JumpForce, Mathf.Sqrt(2 * maxJumpHeight * 9.81f), chargeTime / maxChargeTime);

        // Combinaciones de teclas para diferentes direcciones de salto
        if (Input.GetKey(KeyCode.D))
        {
            if (Input.GetKey(KeyCode.W))
            {
                jumpDirection = (Vector2.up * scaledJumpForce + Vector2.right).normalized; // Diagonal hacia la derecha más vertical
            }
            else
            {
                jumpDirection = (Vector2.up * scaledJumpForce / 2 + Vector2.right * 2).normalized; // Derecha
            }
        }
        else if (Input.GetKey(KeyCode.A))
        {
            if (Input.GetKey(KeyCode.W))
            {
                jumpDirection = (Vector2.up * scaledJumpForce + Vector2.left).normalized; // Diagonal hacia la izquierda más vertical
            }
            else
            {
                jumpDirection = (Vector2.up * scaledJumpForce / 2 + Vector2.left * 2).normalized; // Izquierda
            }
        }

        animator.SetBool("Charging", false);
        animator.SetBool("Jump", true);

        controller.rb.AddForce(jumpDirection * scaledJumpForce, ForceMode2D.Impulse);
        isJumping = true;
        controller.enabled = false;
        chargeTime = 0f; // Resetear tiempo de carga
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            grounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            grounded = false;
        }
    }



}
