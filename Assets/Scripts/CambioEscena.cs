using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;


public class CambioEscena : MonoBehaviour
{
    public string desiredAnimatorControllerName = "NombreDeseado";
    public string sceneNameToLoad = "NombreNivel";

    public UnityEvent eventoEndLevel;

    private void OnTriggerEnter2D(Collider2D other)
    {
        Animator animator = other.GetComponent<Animator>();

        if (animator != null)
        {
            
            string currentAnimatorName = animator.runtimeAnimatorController.name;

          
            if (currentAnimatorName.Equals(desiredAnimatorControllerName))
            {
                eventoEndLevel.Invoke();
                SceneManager.LoadScene(sceneNameToLoad);
            }
        }
    }
    public void CloseApp()
    {
        Application.Quit();
        Debug.Log("Quit");
    }

    public void GoToLevel1()
    {
        SceneManager.LoadScene("Nivel1");
    }

}


   


