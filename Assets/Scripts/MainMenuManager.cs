using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    public GameObject imagenInicial;
    public GameObject menuPrincipal;

    private bool sePresionoTecla = false;

    void Update()
    {
        if (!sePresionoTecla && Input.anyKeyDown)
        {
            CambiarImagen();
            sePresionoTecla = true;
        }
    }

    public void CambiarImagen()
    {
        imagenInicial.SetActive(false);
        menuPrincipal.SetActive(true);
    }

    public void EmpezarJuego(string nombreEscena)
    {
        SceneManager.LoadScene(nombreEscena);
    }

    public void SalirJuego()
    {
        Debug.Log("Saliendo del juego...");
        Application.Quit();
    }
}
