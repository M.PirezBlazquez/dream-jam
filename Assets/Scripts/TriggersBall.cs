using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggersBall : MonoBehaviour
{
    [SerializeField]
    PickupController triggerBall;

    // Start is called before the first frame update
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            triggerBall.isPlayerNear = true;
            Debug.Log("El jugador está cerca");
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            triggerBall.isPlayerNear = false;
            Debug.Log("El jugador ya no está cerca");
        }
    }
}
