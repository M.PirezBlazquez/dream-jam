using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class EndVIdeoLoad : MonoBehaviour
{

    [SerializeField]
    VideoPlayer video;

    public UnityEvent videoEnd;

    // Start is called before the first frame update
    void Start()
    {
        video.aspectRatio = VideoAspectRatio.Stretch;
    }

    // Update is called once per frame
    void Update()
    {
        if(!video.canStep)
        {
            videoEnd.Invoke();
        }

        if(video.isPaused)
        {
            videoEnd.Invoke();
        }
    }

}
